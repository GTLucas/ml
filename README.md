# ML projet



## Compatibilité

Ce programme est compatible avec Ubuntu ainsi que les version 3.11.5 de python et 23.10.1 de conda. Veuillez verifier que vous possedez les bonnes versions python et conda sur votre machine. 


## À-propos

Ce programme modélise l'étude statistque de classification de différentes types de cancer pour plusieurs individus en utilisant une regression logistique ainsi qu'un réseau de neurones. L'objectif est de comparer lequel de ces deux modèles est le plus perfomant sur notre jeu de données.


## Résultats

- Vous pourrez trouver les résultats dans le dossier output du dépôt git. Celui comporte 5 plots dont 3 histogrammes et 2 heatmap pour chacune des matrices de confusion. Vous trouverez dans les histogrammes le nombre de patients par classe de cancer pour l'ensemble du jeu de donnée ainsi que pour les donées d'apprentissage et tests. 
Enfin le rapport présente une analyse des résultats et une explication de la construction du programme. 


## Authors and acknowledgment
Fait par AUBERT Lucas étudiant en M2 DLAD à la faculté des sciences d'aix marseille université AMU. 

