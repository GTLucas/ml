import keras
import pandas as pd
import numpy as np 
import sklearn 
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn import metrics
import seaborn as sns 
import matplotlib.pyplot as plt 

def data_merging(file_1, file_2):
    reader_data = pd.read_csv(filepath_or_buffer= file_1)
    reader_labels = pd.read_csv(filepath_or_buffer= file_2)
    merge = pd.merge(reader_data, reader_labels, on = "Unnamed: 0")
    print(merge)
    return merge

merge = data_merging('data/data.csv', 'data/labels.csv')

#Compter le nombre de modaliter pour la variable qualitative 

def count_modality():
    count = merge["Class"].nunique()
    print(count)
    return count

count = count_modality()

#Plot d'iteration par class sur données initiales
sns.set_style('whitegrid')
sns.countplot(x="Class", data=merge, palette="RdBu_r")
plt.savefig("output/plot_data_class.png")
plt.show()


#info sur les données
merge.info()

#Création des échantillons d'apprentissage et test
def training_data():
    shuffle_data = merge.sample(frac=1)
    X = shuffle_data.iloc[:,1:20532]
    Y = shuffle_data["Class"]
    X_training_sample, X_test_sample, Y_training_sample, Y_test_sample = train_test_split(X, Y, test_size=0.25, random_state=1)
    print(X_training_sample, X_test_sample, Y_training_sample, Y_test_sample)
    return X_training_sample, X_test_sample, Y_training_sample, Y_test_sample

X_training_sample, X_test_sample, Y_training_sample, Y_test_sample = training_data()

#Plot des données d'entrainements 
column_name = ["Class"]
Y_training_sample_pandas = pd.DataFrame(Y_training_sample, columns=column_name)
sns.set_style('whitegrid')
sns.countplot(x="Class", data=Y_training_sample_pandas, palette="RdBu_r")
plt.figure(figsize=(3,3))
plt.savefig("output/plot_data_class_training_sample.png")
plt.show()

#Plot des données test
Y_test_sample_pandas = pd.DataFrame(Y_test_sample, columns=column_name)
sns.set_style('whitegrid')
sns.countplot(x="Class", data=Y_test_sample_pandas, palette="RdBu_r")
plt.figure(figsize=(3,3))
plt.savefig("output/plot_data_class_test_sample.png")
plt.show()

#Normalisation des données d'apprentissages

def normalize():
    result = X_training_sample.iloc[:,1:20532].copy()
    temp = result.sub(result.mean())
    result = temp.div(temp.std())
    return result

result = normalize()

#Normalisation des données test

def normalize_test():
    test_result = X_test_sample.iloc[:,1:20532].copy()
    test_temp = test_result.sub(result.mean())
    test_result = test_temp.div(result.std())
    return test_result

test_result = normalize_test()

#Epuration des données d'apprentissages et tests

def clean_normalize():
    cleaned_result = result.dropna(axis='columns')
    cleaned_test_result = test_result.dropna(axis='columns')
    print(cleaned_result)
    print(cleaned_test_result)
    return cleaned_result, cleaned_test_result

cleaned_result, cleaned_test_result = clean_normalize()



#Afficher la dimensions

def dim_samples():
    cleaned_result_dim = cleaned_result.size
    cleaned_test_result_dim = cleaned_test_result.size
    cleaned_result_column = len(cleaned_result.columns)
    cleaned_test_result_column = len(cleaned_test_result.columns)
    print(cleaned_result_dim, cleaned_test_result_dim, cleaned_result_column, cleaned_test_result_column)
    return cleaned_result_dim, cleaned_test_result_dim, cleaned_result_column, cleaned_test_result_column

cleaned_result_dim, cleaned_test_result_dim, cleaned_result_column, cleaned_test_result_column = dim_samples()

#Encodage automatique pour régression logistique 

def encod():
    LE_preprocessing = LabelEncoder()
    convert_Y_training_sample = LE_preprocessing.fit_transform(Y_training_sample)
    convert_Y_test_sample = LE_preprocessing.fit_transform(Y_test_sample)
    return convert_Y_training_sample, convert_Y_test_sample

convert_Y_training_sample, convert_Y_test_sample = encod()

#Encodage manuel pour réseau de neurones 
##Assigner un encodage à la variable qualitative pour les données d'entrainements 

def unclass_training():
    for row in range(len(Y_training_sample)):
        if Y_training_sample.iloc[row] == "BRCA":
            Y_training_sample.iloc[row] = 0
        if Y_training_sample.iloc[row] == "PRAD":
            Y_training_sample.iloc[row] = 1   
        if Y_training_sample.iloc[row] == "LUAD":
            Y_training_sample.iloc[row] = 2
        if Y_training_sample.iloc[row] == "COAD":
            Y_training_sample.iloc[row] = 3
        if Y_training_sample.iloc[row] == "KIRC":
            Y_training_sample.iloc[row] = 4

    training_sample_qualitative = Y_training_sample
    return training_sample_qualitative

training_sample_qualitative = unclass_training()

def unclass_test():
    for row in range(len(Y_test_sample)):
        if Y_test_sample.iloc[row] == "BRCA":
            Y_test_sample.iloc[row] = 0
        if Y_test_sample.iloc[row] == "PRAD":
            Y_test_sample.iloc[row] = 1
        if Y_test_sample.iloc[row] == "LUAD":
            Y_test_sample.iloc[row] = 2
        if Y_test_sample.iloc[row] == "COAD":
            Y_test_sample.iloc[row] = 3
        if Y_test_sample.iloc[row] == "KIRC":
            Y_test_sample.iloc[row] = 4

    test_sample_qualitative = Y_test_sample
    test_sample_qualitative = np.array(test_sample_qualitative, dtype=object)
    test_sample_qualitative = test_sample_qualitative.astype(int)
    return test_sample_qualitative

test_sample_qualitative = unclass_test()

##Créer une matrice carrée de dimension identique à la variable qualitative

def matrix():
    x = len(X_training_sample)
    y = count
    zeros_matrix = np.zeros((x, y))
    return zeros_matrix

zeros_matrix = matrix()

##On remplie cette matrice de 1 

def one():
    for row in range(len(training_sample_qualitative)):
        zeros_matrix[row, training_sample_qualitative.iloc[row]] = 1 
    return zeros_matrix

zeros_matrix = one()

#modelisation régression logistique
reg1 = LogisticRegression(multi_class= "multinomial")
reg1.fit(cleaned_result, convert_Y_training_sample)
Y_predictions = reg1.predict(cleaned_test_result)

#logistic regression report
print("prédiction: {} ".format(accuracy_score(convert_Y_test_sample, Y_predictions) * 100))
print(classification_report(convert_Y_test_sample, Y_predictions))

#plot confusion matrix regression logistic
Confusion_matrix = confusion_matrix(convert_Y_test_sample, Y_predictions)
sns.heatmap(Confusion_matrix, annot=True, xticklabels= ["BRCA", "PRAD", "LUAD", "COAD", "KIRC"], yticklabels=["BRCA", "PRAD", "LUAD", "COAD", "KIRC",])
plt.figure(figsize=(3,3))
plt.savefig("output/plot_confusion_matrix_logistic_regression.png")
plt.show()


#Modélisation du réseau de neurone 
model1 = keras.models.Sequential()
model1.add(keras.layers.Dense(count, input_dim = (cleaned_result_column), activation= "softmax"))
model1.compile(loss='categorical_crossentropy', optimizer='adam')
model1.fit(cleaned_result, zeros_matrix, epochs=600, batch_size= cleaned_result_column +1)
print("Evaluate:")
model1.evaluate(cleaned_test_result)
model1.save('output/cancer_prediction.h1')
model1.save_weights('output/cancer_prediction_weights.h1')

#réseau de neurone report 
probability_matrix = model1.predict(cleaned_test_result)
max_indices = np.argmax(probability_matrix, axis=1)
print("prédiction: {} ".format(accuracy_score(test_sample_qualitative, max_indices) * 100))
print(classification_report(test_sample_qualitative, max_indices))


#Plot confusion matrix réseau de neurone 
Confusion_matrix_model = confusion_matrix(test_sample_qualitative, max_indices)
sns.heatmap(Confusion_matrix_model, annot=True, xticklabels= ["BRCA", "PRAD", "LUAD", "COAD", "KIRC"], yticklabels=["BRCA", "PRAD", "LUAD", "COAD", "KIRC"])
plt.figure(figsize=(3,3))
plt.savefig("output/plot_confusion_matrix_reseau_neurone.png")
plt.show()

#Charger le modèle 
loaded_model1 = keras.models.load_model('output/cancer_prediction.h1')
loaded_modeld1_weights = model1.load_weights('output/cancer_prediction_weights.h1')
loaded_model1.summary()

